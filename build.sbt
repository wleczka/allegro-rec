name := "allegro-rec"

version := "0.1"

scalaVersion := "2.12.8"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-blaze-client" % "0.20.0",
  "org.http4s" %% "http4s-blaze-server" % "0.20.0",
  "org.http4s" %% "http4s-dsl" % "0.20.0"
)

scalacOptions ++= Seq("-Ypartial-unification")